
const services = {};

function init () {
    // Don't reinitialize
    if (Object.keys(services).length > 0) {
        return;
    }
    services.totalBalanceService = require('./totalBalanceService/service');
    
    return services;
}

module.exports = {init};
