async function validateETHAddresses(addresses) { 
    let result = true;
    addresses.forEach(address => {
        if (!(/^(0x){1}[0-9a-fA-F]{40}$/i.test(address)))
            result = false;
    });
    return result;
}

module.exports = {
    validateETHAddresses
}