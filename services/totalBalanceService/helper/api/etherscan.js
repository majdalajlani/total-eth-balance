const axios = require('axios');
const logger = require('../../../../config/logger');

const ORIGINS = {
    KOVAN:          process.env.ETHEREUM_TESTNET_GOERLI_API_URL,
    RINKEBY:        process.env.ETHEREUM_TESTNET_GOERLI_API_URL,
    ROPSTEN:        process.env.ETHEREUM_TESTNET_GOERLI_API_URL,
    GOERLI:         process.env.ETHEREUM_TESTNET_GOERLI_API_URL,
    PRODUCTION:     process.env.ETHEREUM_TESTNET_GOERLI_API_URL,
};

const ORIGIN = ORIGINS[process.env.ETHEREUM_NETWORK_MODE];

const API_KEYS = {
    KOVAN:          process.env.ETHEREUM_TESTNET_KOVAN_API_KEY,
    RINKEBY:        process.env.ETHEREUM_TESTNET_RINKEBY_API_KEY,
    ROPSTEN:        process.env.ETHEREUM_TESTNET_ROPSTEN_API_KEY,
    GOERLI:         process.env.ETHEREUM_TESTNET_GOERLI_API_KEY,
    PRODUCTION:     process.env.ETHEREUM_PRODUCTION_API_KEY,
};

const API_KEY = API_KEYS[process.env.ETHEREUM_NETWORK_MODE];


async function makeRequest(url, module, action, params) {
    try {
        
    const payload = {
        module,
        action,
        tag: 'latest',
        apikey: API_KEY,
        ...params,
    };
    
    const header = {
        'Accept-Encoding': 'application/json',
    };

    const response = await axios.get(url, { params: payload, headers: header });

    return response.data;
    } catch (e) {
        logger.error(`makeRequest: URL: ${url} Payload:${payload} Error:${e.message || e}`);
        throw e;
    }
}


async function getAddressesBalance(address) { 
    return await makeRequest(
        `${ORIGIN}/`,
        'account',
        'balancemulti',
        {address}
    );
}

module.exports = {
    getAddressesBalance
}