const { ErrorHandler } = require("../../config/errorHandler");
const { HTTP_STATUS_CODES } = require("../../routers/helpers/httpStatusCodes");
const { getAddressesBalance } = require("./helper/api/etherscan");
const { validateETHAddresses } = require("./helper/validator");
const API_STRINGS = require('./../../config/apiStrings.json');
const logger = require("../../config/logger");

async function getTotalBalance(addresses) {
    try {
        // Check if addresses array exists 
        if (!addresses || addresses.length < 1) {
            logger.warn('The addresses parameter should have 1 address at least');
            
            throw new ErrorHandler(HTTP_STATUS_CODES.BAD_REQUEST, API_STRINGS.totalBalance.error_invalid_addresses_field);
        }

        let addressesAsArray = addresses.split(',');

        // Validate if addresses more than 100 addresses
        if (addressesAsArray.length > 100) {
            logger.warn('The user exceed the address limit per request');
            
            throw new ErrorHandler(HTTP_STATUS_CODES.BAD_REQUEST, API_STRINGS.totalBalance.error_invalid_number_of_addresses);

        }

        // Validate all ETH addresses
        if (! await validateETHAddresses(addressesAsArray)) {
            logger.warn('Invalid ETH addresses');
            
            throw new ErrorHandler(HTTP_STATUS_CODES.BAD_REQUEST, API_STRINGS.totalBalance.error_invalid_eth_address);
        }

        // Get total balance from etherscan 20 elements in each fetch
        let addressesWithBalance = [];

        while (addressesAsArray.length) {
            const batchOfAddresses = addressesAsArray.splice(0, 20);
            const totalBalanceForEachAddress = await getAddressesBalance(batchOfAddresses.join());

            addressesWithBalance = addressesWithBalance.concat(totalBalanceForEachAddress.result.map(addressBalance => {
                return {
                    address: [addressBalance.account], // It should be an Object but based on the doc I make it as array
                    balance: [addressBalance.balance] // Same above ^^ 
                };
            }));
        }

        // Sum of total balance for all addresses
        const totalBalance =  addressesWithBalance.reduce((accumulator, object) => {
            return accumulator + parseInt(object.balance);
        }, 0);
        
        return {
            addresses: addressesWithBalance,
            totalBalance
        };
    } catch (e) {
        logger.error(e.message || e);
        throw e;
    }
}
    
module.exports = {
    getTotalBalance,
};
