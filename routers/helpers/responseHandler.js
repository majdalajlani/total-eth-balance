const { HTTP_STATUS_CODES } = require("./httpStatusCodes");
const { ErrorHandler } = require("./../../config/errorHandler");
const API_STRINGS = require('../../config/apiStrings.json');

function handleResponse(data, res) {
    if (!data) {
        res.status(HTTP_STATUS_CODES.BAD_REQUEST).json({
            status: "error",
            statusCode: HTTP_STATUS_CODES.BAD_REQUEST,
            data: 'No Data'
        });
    } else if (!data.error) {
        res.status(HTTP_STATUS_CODES.OK).json({
            status: "success",
            statusCode: HTTP_STATUS_CODES.OK,
            data
        });
    } else {
        throw new ErrorHandler(HTTP_STATUS_CODES.INTERNAL_SERVER, API_STRINGS.general.error);
    }
}

module.exports = {
    handleResponse
}