module.exports = (services, app) => {
    require('./totalBalanceRouter')(services, app);
    require('./checkHealthRouter')(services, app);
};
