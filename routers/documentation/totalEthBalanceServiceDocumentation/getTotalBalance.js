module.exports = {
    get:{
        tags:['Total Eth Balance Service'],
        description: "Total Eth Balance Service",
        operationId: "getTotalBalance",
        parameters:[
            {
                name:"addresses",
                in:"path",
                schema:{
                   type: "array",
                },
                required:true,
                description: "An array of ETH addresses"
            }
        ],
        responses:{
            '200':{
                description:"Total balance is obtained",
                content:{
                    'application/json':{
                        schema:{}
                    }
                }
            },
            '400':{
                description: "There is an issue",
                content:{
                    'application/json':{
                        schema:{}
                    }
                }
            }
        }
    }
}
