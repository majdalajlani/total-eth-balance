const getTotalBalanceDoc = require('./getTotalBalance');

module.exports = {
    paths:{
        '/api/v1/get-total-balance/':{
            ...getTotalBalanceDoc,
        },
    }
}