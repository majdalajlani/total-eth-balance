const basicInfo = require('./basicInfo');
const servers = require('./servers');
const tags = require('./tags');
const totalEthBalanceServiceDocumentation = require('./totalEthBalanceServiceDocumentation');

module.exports = {
    ...basicInfo,
    ...servers,
    ...tags,
    ...totalEthBalanceServiceDocumentation
};