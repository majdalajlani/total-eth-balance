module.exports = {
    openapi:"3.0.1",
    info:{
        version:"1.0.0",
        title:"Total Eth Balance Service",
        description: "Total Eth Balance Service",
        contact:{
            name:"Majd Al Ajlani",
            email:"majdalajlani@gmail.com",
            url:"https://majdalajlani.com"
        }
    }
}