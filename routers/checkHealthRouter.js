const { handleResponse } = require('./helpers/responseHandler');
const API_STRINGS = require('./../config/apiStrings.json');

module.exports = (services, app) => {
    app.get('/api/v1/healthcheck', (req, res) => {
        handleResponse({ server_status: API_STRINGS.health_check.success }, res);
    });
};
