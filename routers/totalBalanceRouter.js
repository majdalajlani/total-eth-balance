const { handleError } = require("../config/errorHandler");
const logger = require("../config/logger");
const { handleResponse } = require("./helpers/responseHandler");

module.exports = (services, app) => {

    app.get(
        '/api/v1/get-total-balance/',
        async (req, res) => {
            try {
                const response = await services.totalBalanceService.getTotalBalance(req.query.addresses);
                handleResponse(response, res);
            } catch (e) {
                handleError(e, res);
            }
        }
    );
};
