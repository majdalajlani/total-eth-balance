
# Total ETH Balance Service

A service to fetch the total balance of each address from etherscan and calculate the total balances of the addresses.


## Installation

Install totalBalanceService with npm

```bash
  git clone https://gitlab.com/majdalajlani/total-eth-balance.git
  cd totalBalanceService
  npm install 
  cp .env.template .env
  npm run start or pm2 start
```
    
## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

Create [etherscan API key](https://docs.etherscan.io/getting-started/viewing-api-usage-statistics)
 `ETHEREUM_TESTNET_GOERLI_API_KEY`

`ETHEREUM_TESTNET_GOERLI_API_URL`, `ETHEREUM_NETWORK_MODE`, `PORT`, `NODE_ENV`


## Running Tests

To run tests, run the following command

```bash
  npm run test
```


## Features

- CORS enabled
- Uses `npm`
- Expressjs framework
- Consistent coding styles with editorconfig
- Docker support
- Load environment variables from .env files with dotenv
- Linting with eslint
- Tests with Jest
- Logging with morgan
- API documentation generation with swagger
- Monitoring with pm2

## Documentation

[Documentation](https://totalethbalance.majdalajlani.com/api-docs/)


## Tech Stack

**Server:** Node, Express

**Editor:** Node (v14.15.4), NPM (6.14.10), Express

## Support

For support, email majdalajlani@gmail.com

Developed by Majd Al Ajlani