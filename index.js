require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 3000;

const logger = require('./config/logger');
const morganMiddleware = require("./config/morgan.middleware");

const { handleError } = require('./config/errorHandler');

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Add the morgan middleware
app.use(morganMiddleware);

// Prepare services
const services = require('./services/index').init();
// Inject services to routers 
require('./routers/index')(services, app);



const swaggerUI = require("swagger-ui-express");
const docs = require('./routers/documentation/');

app.use('/api-docs',swaggerUI.serve,swaggerUI.setup(docs));

app.use((err, req, res, next) => {
  handleError(err, res);
});


module.exports = app.listen(port, () => logger.info(`TotalBalanceService listening on port ${port}!`));