function ErrorHandler(statusCode, message) {
    return {statusCode, message}
}

const handleError = (err, res) => {
  const { statusCode, message } = err;
  res.status(statusCode).json({
    status: "error",
    statusCode,
    message
  });
};

module.exports = {
    ErrorHandler,
    handleError
}
